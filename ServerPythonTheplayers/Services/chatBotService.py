#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 23 16:42:41 2019

@author: gibbygarcia
"""
from Utils.chatBotUtils import *
from Persistence.Data.jsonData import intentsResponses_JsonData
from Persistence.Data.templatesData import *
from Persistence.DBModel.unitisStoreModel import *

def dispatcher_Service(req, contexto):
    intent = getIntentName_FromDlgFlwRequest_Util(req)
    
    switcher = {
        'BuscarArticulo':searchArticle_Service,
        'AgregarArticuloCarrito2params-SelectNumber': addToCart_Service,
        'VerCarritoCompras': showCart_Service
        
    }
    func = switcher.get(intent, None)
    if func != None:
        return func(req, contexto)
    
def simpleResponse_Service(req, contexto):
    intent = getIntentName_FromDlgFlwRequest_Util(req)
    return getResponseToSendFromJSON_Util(intentsResponses_JsonData(), intent)

def searchArticle_Service(req, contexto):
    subcategoria = getParamFromDlgFlwRequest_Util(req).get('option-subcategoria')
    categoria = getParamFromDlgFlwRequest_Util(req).get('option-categoria')
    articulos = getParamFromDlgFlwRequest_Util(req).get('option-articulos')
    colores = getParamFromDlgFlwRequest_Util(req).get('option-colores')
    marcas = getParamFromDlgFlwRequest_Util(req).get('option-marcas')
    submarca = getParamFromDlgFlwRequest_Util(req).get('option-submarca')
    tamanio = getParamFromDlgFlwRequest_Util(req).get('option-tamanio')
     
    result = searchArticulo_APIModel(subcategoria,categoria,articulos,colores,marcas,submarca,tamanio)
    return  result
    
def addToCart_Service(req, contexto):
    cantidad = getParamOutPutContextFromDlgFlwRequest_Util(req).get('cantidad') 
    idarticulo = getParamOutPutContextFromDlgFlwRequest_Util(req).get('id-articulo') 
    print(' cantidad: '+str(cantidad)+' idarticulo: '+str(idarticulo))  
    result = addToCart_APIModel(idarticulo,cantidad)  
    #response = 'He agregado el articulo solicitado :)' 
    response = cardResponse2_JsonTemplate()  
    print(result) 
    if result is False:
        response = 'El artículo que quieres agregar no es válido :('
        return generateResponseTemplate_Util(response)
    if result is True:
        return cardResponse2_JsonTemplate() 
    

def showCart_Service(req, contexto):   
    idcarrito=1
    carrito = getCart_APIModel(idcarrito)
    print(carrito)
    
    #result = sendPayment_PayPal()  
    #print(result) 
    #url = result.get('links')[1].get('href') 
    url = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=EC-7X5061366Y4293546"
    print('hhhhhhhhhhhhhhhhhhh')    
    return generateResponseCustom2PlayloadTemplate_Util(carrito,'',buttonTemplate(url),'')   
 
    