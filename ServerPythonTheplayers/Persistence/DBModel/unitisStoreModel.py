#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 23 17:14:15 2019

@author: gibbygarcia
"""
import pymysql
import os
import datetime
import json
import requests
from Persistence.Data.jsonData import loginCredentials_JsonData

apiChatbot = "http://localhost:8080"

# -----------------Examples how to connect with credentials--------------
#def getAccessToken_APIModel():
#    url=apiPartstech+"/oauth/access"
#    payload = loginCredentials_JsonData()
#    headers = {'content-type': 'application/json'}
#    r = requests.post(url, data=json.dumps(payload), headers=headers)
#    return r.json().get("accessToken")

#def getPartTypes_APIModel():
#    url = apiPartstech+"/taxonomy/part-types"
#    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
#    r = requests.get(url, headers=headers)
#    return r.json()

#def getCart_APIModel(sessionid):
#    url = apiPartstech+"/punchout/cart/info"
#    payload = {
#      "sessionId": sessionid
#    } 
#    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
#    r = requests.post(url, data=json.dumps(payload), headers=headers) 
#    return r.json() 
# -----------------END xamples how to connect with credentials--------------

def searchArticulo_APIModel(subcategoria,categoria,articulos,colores,marcas,submarca,tamanio):
    url = apiChatbot+"/gs_searcharticulos" 
    payload = {
      "subcategoria": subcategoria,
      "categoria": categoria,
      "articulos": articulos,
      "colores": colores,
      "marcas": marcas,
      "submarca": submarca,
      "tamanio": tamanio
    } 
    #print(payload.content)   
    headers = {'content-type': 'application/json'} 
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    r = r.json() 
    return r 

def addToCart_APIModel(idArticulo,cantidad): 
    url = apiChatbot+"/gs_addToCart"
    payload = { 
      "idarticulo": idArticulo,
      "cantidad": cantidad 
    }  
    #print(payload.content)   
    headers = {'content-type': 'application/json'} 
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    r = r.json()  
    return r.get('estatus')

def getCart_APIModel(idcarrito):
    url = apiChatbot+"/gs_getCart"  
    payload = {"idcarrito": idcarrito}   
    headers = {'content-type': 'application/json'}  
    r = requests.get(url, params=payload, headers=headers)  
    return r.json()
