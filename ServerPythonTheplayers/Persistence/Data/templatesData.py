#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 23 18:29:24 2019

@author: gibbygarcia
"""

def cardResponse_JsonTemplate():
    card = {
  "fulfillmentText": " hjhvh",
  "fulfillmentMessages": [
    {
      "card": {
        "title": "Selecciona una opción:",
        "subtitle": "Ticket #{1}",
        "buttons": [
          {
            "text": "Clic aquí para descargar factura",
            "postback": "http://mexisign.com/DBimg/Ejemplo-de-factura.jpg"
          },
          {
            "text": "Visita nuestro sitio web",
            "postback": "http://unitis.com.mx/"
          }
        ]
      }
    }
  ]
}
    return card


def cardResponse2_JsonTemplate():
    card = {
  "fulfillmentMessages": [
    {
      "text": {
        "text": [
          "He agregado el articulo solicitado",
          " :) "
        ]
      }
    },
    {
      "quickReplies": { 
        "title": "¿Quieres ver los artículos que has seleccionado?",     
        "quickReplies": [
          "Ver lista de compras"
        ]
      }
    }
  ]
}
    return card 

def generateResponseCustom2PlayloadTemplate_Util(playload, texto, playload2, texto2):
    return {'fulfillmentMessages': [{
    'payload': {
        "facebook": {
            "attachment": {
                "type": "template",
                "payload": playload
            }
        }
    }
    }, {
      "text": {
        "text": [texto]
      }
    },
    {
    'payload': {
        "facebook": {
            "attachment": {
                "type": "template",
                "payload": playload2
            }
        }
    }
    }, {
      "text": {
        "text": [texto2]
      }
    },]}

def buttonTemplate(url):
    result = {
    "template_type":"button",
        "text":"¿Quieres continuar con el pago?",
        "buttons":[
          {
            "type":"web_url",
            "url":url,
            "title":"Ir a Pagar"
          }]}
    return result
