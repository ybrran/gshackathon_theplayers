#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 23 17:04:01 2019

@author: gibbygarcia
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 20 08:27:39 2019

@author: gibbygarcia
"""

def intentsResponses_JsonData():
    responses= [
	{
		"id": 1,
		"intent": "nombre",
        "responses":[
             {'fulfillmentText': 'Mi nombre es Chatbot, y estoy programado para ayudarte con cualquier duda, modificación de compra o facturación.'}      
         ] 
	},{
		"id": 2,
		"intent": "Gracias",
        "responses":[
             {'fulfillmentText': 'Estoy para servirte :D'},
             {'fulfillmentText': 'A tus órdenes! :D'}    
         ]
	}
  
]
    return responses


def loginCredentials_JsonData():
    credentials= {
  "accessType": "user",
  "credentials": {
    "user": {
      "id": "hackteam_17",
      "key": "70e8c0e71d3445bd8eed7b453641a4e4"
    },
    "partner": {
      "id": "beta_bosch",
      "key": "4700fc1c26dd4e54ab26a0bc1c9dd40d"
    }
  }
}
    return credentials

def cardResponse2_JsonTemplate():
    card = {
  "fulfillmentMessages": [
    {
      "text": {
        "text": [
          "He agregado el articulo solicitado",
          " :) "
        ]
      }
    },
    {
      "quickReplies": { 
        "title": "¿Quieres ver los artículos que has seleccionado?",     
        "quickReplies": [
          "Ver lista de compras"
        ]
      }
    }
  ]
}
    return card 

def generateResponseCustom2PlayloadTemplate_Util(playload, texto, playload2, texto2):
    return {'fulfillmentMessages': [{
    'payload': {
        "facebook": {
            "attachment": {
                "type": "template",
                "payload": playload
            }
        }
    }
    }, {
      "text": {
        "text": [texto]
      }
    },
    {
    'payload': {
        "facebook": {
            "attachment": {
                "type": "template",
                "payload": playload2
            }
        }
    }
    }, {
      "text": {
        "text": [texto2]
      }
    },]}