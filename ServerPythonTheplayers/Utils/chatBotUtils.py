#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 23 16:49:31 2019

@author: gibbygarcia
"""
import random
import json


def getIntentName_FromDlgFlwRequest_Util(req):
    return req.get('queryResult').get('intent').get("displayName")

def getImageAttached_Util(req):
    return req.get('originalDetectIntentRequest').get('payload').get('data').get('message').get('attachments')[0].get('payload').get('url')

def getTxtMessageFromDlgFlwRequest_Util(req):
    return req.get('queryResult').get('queryText')

def getParamFromDlgFlwRequest_Util(req):
    return req.get('queryResult').get('parameters')

def getParamOutPutContextFromDlgFlwRequest_Util(req):
    return req.get('queryResult').get('outputContexts')[0].get('parameters')

def getParamOutPutContextFromDlgFlwRequest_Util_level2(req):
    return req.get('queryResult').get('outputContexts')[1].get('parameters')

def getSenderIdFromDlgFlwRequest_Util(req):
    return req.get('queryResult').get('outputContexts')[0].get('parameters').get('facebook_sender_id')

def getResponseToSendFromJSON_Util(jsonObject, intent): 
    for obj in jsonObject:
      if obj.get("intent") == intent:
          return random.choice(obj.get("responses"))

def generateResponseTemplate_Util(response):
    return {'fulfillmentText': response}

def generateResponseCustomPlayloadTemplate_Util(playload, texto):
    return {'fulfillmentMessages': [{
    'payload': {
        "facebook": {
            "attachment": {
                "type": "template",
                "payload": playload
            }
        }
    }
    }, {
      "text": {
        "text": [texto]
      }
    },]}
    
def generateResponseCustom2PlayloadTemplate_Util(playload, texto, playload2, texto2):
    return {'fulfillmentMessages': [{
    'payload': {
        "facebook": {
            "attachment": {
                "type": "template",
                "payload": playload
            }
        }
    }
    }, {
      "text": {
        "text": [texto]
      }
    },
    {
    'payload': {
        "facebook": {
            "attachment": {
                "type": "template",
                "payload": playload2
            }
        }
    }
    }, {
      "text": {
        "text": [texto2]
      }
    },]}