#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 22:02:53 2019

@author: gibbygarcia telcel -> 
"""
import os

from flask import Flask, jsonify, request
from Services.chatBotService import dispatcher_Service
from Utils.chatBotUtils import *

app = Flask(__name__)
contexto="https://3a20650c.ngrok.io"
 
@app.route("/", methods=["POST"])
def chatbot(): 
    req = request.get_json(silent=True, force=True)
    print(req) 
      
    response = dispatcher_Service(req, contexto)
    #return "hey python GSHackathon home" 
    return jsonify(response) 
 
if __name__ == '__main__':
    port = int(os.getenv('PORT', 5001)) 
    app.run(debug=True, port=port, host='127.0.0.1')  