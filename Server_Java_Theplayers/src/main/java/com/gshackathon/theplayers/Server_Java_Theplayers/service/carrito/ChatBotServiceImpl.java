package com.gshackathon.theplayers.Server_Java_Theplayers.service.carrito;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gshackathon.theplayers.Server_Java_Theplayers.dao.ChatBotMyBatisDAO;
import com.gshackathon.theplayers.Server_Java_Theplayers.dao.U01_CarritoMyBatisDAO;
import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates.ReceiptVO;
import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates.TemplatesUtils;
import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates.U01_FFButtonTemplateVO;
import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates.U01_FFCardTemplateVO;
import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates.U01_FFMessageVO;
import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates.U01_FFResponseVO;
import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.vo.ArticulosVO;

@Service("ChatBotService")
public class ChatBotServiceImpl implements ChatBotService {

	@Autowired
	ChatBotMyBatisDAO chatBotMyBatisDAO;
	
	@Autowired
	U01_CarritoMyBatisDAO u01_CarritoMyBatisDAO;
	
	@Override
	public List<Long> getAllEmpresas() {
		List<Long> empresas = chatBotMyBatisDAO.getEmpresas();
		
		return empresas;
	}
	
	@Override
	public U01_FFResponseVO searchInInventario(String subcategoria,
			String categoria,String articulos,String colores,
			String marcas,String submarca,String tamanio) {
		
		List<ArticulosVO> listarticles = new ArrayList<ArticulosVO>();
		List<ArticulosVO> articulosResult = new ArrayList<ArticulosVO>();
		
		listarticles = chatBotMyBatisDAO.searchInInventario();
		
		int[] optiones = new int[listarticles.size()];
		int coincidencias=0;
		for(int i=0;i<listarticles.size();i++) {
			
			if(hasValue(listarticles.get(i).getSubcategoria())) {
				String subcat =listarticles.get(i).getSubcategoria().toUpperCase();
				if(subcat.equals(subcategoria.toUpperCase())) {
					listarticles.get(i).setInsearch(listarticles.get(i).getInsearch()+1);
					coincidencias++;
				}
			}
			if(hasValue(listarticles.get(i).getCategoria())) {
				String categoriaArt =listarticles.get(i).getCategoria().toUpperCase();
				if(categoriaArt.equals(categoria.toUpperCase())) {
					listarticles.get(i).setInsearch(listarticles.get(i).getInsearch()+1);
					coincidencias++;
				}
			}
			if(hasValue(listarticles.get(i).getArticulos())) {
				String articuloArt =listarticles.get(i).getArticulos().toUpperCase();
				if(articuloArt.equals(articulos.toUpperCase())) {
					listarticles.get(i).setInsearch(listarticles.get(i).getInsearch()+1);
					coincidencias++;
				}
			}
			if(hasValue(listarticles.get(i).getColores())) {
				String colorArt =listarticles.get(i).getColores().toUpperCase();
				if(colorArt.equals(colores.toUpperCase())) {
					listarticles.get(i).setInsearch(listarticles.get(i).getInsearch()+1);
					coincidencias++;
				}
			}
			if(hasValue(listarticles.get(i).getMarcas())) {
				String marcaArt =listarticles.get(i).getMarcas().toUpperCase();
				if(marcaArt.equals(marcas.toUpperCase())) {
					listarticles.get(i).setInsearch(listarticles.get(i).getInsearch()+1);
					coincidencias++;
				}
			}
			if(hasValue(listarticles.get(i).getSubmarca())) {
				String submarcaArt =listarticles.get(i).getSubmarca().toUpperCase();
				if(submarcaArt.equals(submarca.toUpperCase())) {
					listarticles.get(i).setInsearch(listarticles.get(i).getInsearch()+1);
					coincidencias++;
				}
			}
			if(hasValue(listarticles.get(i).getTamanio())) {
				String tamanioArt =listarticles.get(i).getTamanio().toUpperCase();
				if(tamanioArt.equals(tamanio.toUpperCase())) {
					listarticles.get(i).setInsearch(listarticles.get(i).getInsearch()+1);
					coincidencias++;
				}
			}
			optiones[i]=coincidencias;
			coincidencias=0;
		}
		
		for(int i=0;i<listarticles.size();i++) {
			if(listarticles.get(i).getInsearch()>0){
				articulosResult.add(listarticles.get(i));
			}
		}
		U01_FFResponseVO articulosresponse = makeFormatForArticles(articulosResult);
		return articulosresponse;
	}
	
	public Boolean hasValue(String value) {
		if(value!=null) {
			if(value.trim()!="") {
				return true;
			}
		}
		return false;
	}
	
	public U01_FFResponseVO makeFormatForArticles(List<ArticulosVO> articulos) {
		U01_FFResponseVO response = new U01_FFResponseVO();
		List<U01_FFMessageVO> messages = new ArrayList<U01_FFMessageVO>();
		U01_FFMessageVO message = null;
		U01_FFCardTemplateVO card = null;
		
		for(int i=0; i<articulos.size();i++) {
			
			message = new U01_FFMessageVO();
			card = new U01_FFCardTemplateVO();
			List<U01_FFButtonTemplateVO> buttons = new ArrayList<U01_FFButtonTemplateVO>();
			U01_FFButtonTemplateVO buttonAgregarCart = new U01_FFButtonTemplateVO();
			U01_FFButtonTemplateVO buttonShowImage = new U01_FFButtonTemplateVO();
			
			buttonShowImage.setType("postback");
			buttonShowImage.setText("Ver imagen completa");
			buttonShowImage.setPostback(articulos.get(i).getUrl_imagen());
			
			buttonAgregarCart.setType("postback");
			buttonAgregarCart.setText("Agregar el # "+articulos.get(i).getIdarticulo());
			buttonAgregarCart.setPostback(null);
			
			buttons.add(buttonAgregarCart);
			buttons.add(buttonShowImage);
			
			card.setTitle(articulos.get(i).getArticulos());
			card.setImageUri(articulos.get(i).getUrl_imagen());
			card.setSubtitle("Precio: $"+articulos.get(i).getPrecio()+
					" \n Talla: "+articulos.get(i).getTamanio()+
					" \n Id: "+articulos.get(i).getIdarticulo());
			card.setButtons(buttons);			
			message.setCard(card);
			
			messages.add(message);
		}
		response.setFulfillmentText("Lista de artículos:");
		response.setFulfillmentMessages(messages);
		return response;
	}

	@Override
	public boolean addToCart(Long idarticulo, Long cantidad, Long idcarrito) {
		ArticulosVO articulo = chatBotMyBatisDAO.getArtById(idarticulo);
		if(articulo!=null) {
			ArticulosVO articuloexist = chatBotMyBatisDAO.articleIsOnCart(idarticulo, idcarrito);
			if(articuloexist!=null) {
				Long cantidadFinal=articuloexist.getCantidad()+cantidad;
				u01_CarritoMyBatisDAO.updateCarrito(idarticulo,cantidadFinal);
			}else {
				u01_CarritoMyBatisDAO.addToCart(idarticulo,cantidad);
			}
			
			return true;
		}
		return false;
		 
	}
	
	@Override
	public ReceiptVO getCart(Long idcarrito) {
		List<ArticulosVO> articulos = new ArrayList<ArticulosVO>();
		articulos = u01_CarritoMyBatisDAO.getCart(idcarrito);
		//String articulosresponse = makeFormatForCart(articulos);
		TemplatesUtils utilidadestemplate = new TemplatesUtils();
		ReceiptVO ticket= utilidadestemplate.makeReceiptFormat(1L, null,articulos);
		return ticket;
	}
	
	
}
