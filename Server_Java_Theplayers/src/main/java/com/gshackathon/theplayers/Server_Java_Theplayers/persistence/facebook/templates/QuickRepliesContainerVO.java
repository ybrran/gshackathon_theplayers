package com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates;

public class QuickRepliesContainerVO {
	private QuickRepliesStringAttayVO quickReplies;

	public QuickRepliesStringAttayVO getQuickReplies() {
		return quickReplies;
	}

	public void setQuickReplies(QuickRepliesStringAttayVO quickReplies) {
		this.quickReplies = quickReplies;
	}
	
	
}
