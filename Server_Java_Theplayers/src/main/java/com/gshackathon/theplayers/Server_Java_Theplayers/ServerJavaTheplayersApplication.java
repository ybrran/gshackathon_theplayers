package com.gshackathon.theplayers.Server_Java_Theplayers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerJavaTheplayersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerJavaTheplayersApplication.class, args);
	}

}
