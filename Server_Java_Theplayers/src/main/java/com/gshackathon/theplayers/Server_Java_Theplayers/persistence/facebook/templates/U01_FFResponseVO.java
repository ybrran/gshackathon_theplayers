package com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates;

import java.util.List;

public class U01_FFResponseVO {
	private String fulfillmentText;
	private List<U01_FFMessageVO> fulfillmentMessages;
	
	
	public String getFulfillmentText() {
		return fulfillmentText;
	}
	public void setFulfillmentText(String fulfillmentText) {
		this.fulfillmentText = fulfillmentText;
	}
	public List<U01_FFMessageVO> getFulfillmentMessages() {
		return fulfillmentMessages;
	}
	public void setFulfillmentMessages(List<U01_FFMessageVO> fulfillmentMessages) {
		this.fulfillmentMessages = fulfillmentMessages;
	}
	
	
	
}
