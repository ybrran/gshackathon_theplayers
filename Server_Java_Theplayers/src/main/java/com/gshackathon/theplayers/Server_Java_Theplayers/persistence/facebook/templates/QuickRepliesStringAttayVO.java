package com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates;

import java.util.List;

public class QuickRepliesStringAttayVO {
	private String title;
	private List<String> quickReplies;

	
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getQuickReplies() {
		return quickReplies;
	}

	public void setQuickReplies(List<String> quickReplies) {
		this.quickReplies = quickReplies;
	}
	
	
}
