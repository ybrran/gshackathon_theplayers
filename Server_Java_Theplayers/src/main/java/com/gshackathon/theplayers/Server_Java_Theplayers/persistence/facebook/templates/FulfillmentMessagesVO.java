package com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates;

import java.util.List;

public class FulfillmentMessagesVO {
	private List<Object> FulfillmentMessages;

	public List<Object> getFulfillmentMessages() {
		return FulfillmentMessages;
	}

	public void setFulfillmentMessages(List<Object> fulfillmentMessages) {
		FulfillmentMessages = fulfillmentMessages;
	}
	
	
}
