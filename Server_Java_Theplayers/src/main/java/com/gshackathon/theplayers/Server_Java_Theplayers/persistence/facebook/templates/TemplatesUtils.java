package com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates;

import java.util.ArrayList;
import java.util.List;

import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.vo.ArticulosVO;

public class TemplatesUtils {
	
	public ReceiptVO makeReceiptFormat(Long idcliente, List<AdjustmentsVO> descuentos,
			List<ArticulosVO> articulos) {
		double costo_envio=100;
		double iva=0.15;
		
		double subtotal=0;
		List<ElementsReceiptVO> elementos = new ArrayList<ElementsReceiptVO>();
		ElementsReceiptVO elemento = null;
		if(elementos!=null) {
			for(int i=0;i<articulos.size();i++) {
				
				elemento= new  ElementsReceiptVO();
				
				elemento.setTitle(articulos.get(i).getArticulos());
				elemento.setSubtitle(null);
				elemento.setCurrency("MXN");
				elemento.setImage_url(articulos.get(i).getUrl_imagen());
				elemento.setPrice(articulos.get(i).getPrecio());
				elemento.setQuantity(articulos.get(i).getCantidad());
				
				Long cantidad=articulos.get(i).getCantidad();
				double precio=articulos.get(i).getPrecio();
				subtotal = subtotal+(cantidad*precio);
				
				elementos.add(elemento);
			}
		}
		
		double cantidad_descuento=0;
		if(descuentos!=null) {
			for(int i=0;i<descuentos.size();i++) {
				cantidad_descuento=cantidad_descuento+descuentos.get(i).getAmount();	
			}
		}
		double total = subtotal-cantidad_descuento;		
		double total_impuestos=total*iva;
		total=total+total_impuestos+costo_envio;
		
		SummaryVO detallespago = new SummaryVO();
		detallespago.setSubtotal(subtotal);
		detallespago.setShipping_cost(costo_envio);
		detallespago.setTotal_tax(total_impuestos);
		detallespago.setTotal_cost(total);
		
		AddressVO direccion = new AddressVO();
		ReceiptVO ticket = new ReceiptVO();
		if(idcliente!=null) {
			direccion.setCity("CDMX");
			direccion.setCountry("MX");
			direccion.setPostal_code("94740");
			direccion.setState("Estado de México");
			direccion.setStreet_1("Calle Juan de Dios Peza 2, Col. Civer");
			
			ticket.setRecipient_name("Gibran Garcia");
			
			
		}
		
		ticket.setOrder_number("12345678902");
		ticket.setCurrency("MXN");
		ticket.setPayment_method("En curso");
		ticket.setOrder_url("http://petersapparel.parseapp.com/order?order_id=123456");
		ticket.setTimestamp("1428444852");
		
		ticket.setAddress(direccion);
		ticket.setAdjustments(descuentos);
		ticket.setElements(elementos);
		ticket.setSummary(detallespago);
		
		return ticket;
		
	}
	
	public FulfillmentMessagesVO makeQuickReplies(List<String> textosprevios, String titulo, List<String> respuestas) {
		FulfillmentMessagesVO full = new FulfillmentMessagesVO();
		
		//Añadimos los textos previos
		TextContainerVO containertext = new TextContainerVO();
		TextArrayVO arraytext = new TextArrayVO();
		List<String> textos = null;
		if(textosprevios!=null) {
			textos = new ArrayList<String>();
			for(int i=0;i<textosprevios.size();i++) {
				textos.add(textosprevios.get(i));
			}
		}
		

		arraytext.setText(textos);
		containertext.setText(arraytext);

		//Añadimos los quick replies
		QuickRepliesContainerVO replycontainer = new QuickRepliesContainerVO();
		QuickRepliesStringAttayVO quickreply = new QuickRepliesStringAttayVO();
		List<String> text_replyes = null;
		
		if(respuestas!=null) {
			text_replyes = new ArrayList<String>();
			for(int i=0;i<respuestas.size();i++) {
				text_replyes.add(respuestas.get(i));
			}
		}
		
		
		
		quickreply.setTitle(titulo);
		quickreply.setQuickReplies(text_replyes);
		replycontainer.setQuickReplies(quickreply);
		
		//creamos la respuesta
		List<Object> list = new ArrayList<Object>();
		list.add(containertext);
		list.add(replycontainer);
		full.setFulfillmentMessages(list);
				
		return full;
	}
	
}
