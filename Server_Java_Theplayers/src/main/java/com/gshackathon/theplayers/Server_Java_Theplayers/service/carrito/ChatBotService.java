package com.gshackathon.theplayers.Server_Java_Theplayers.service.carrito;

import java.util.List;

import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates.ReceiptVO;
import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates.U01_FFResponseVO;
import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.vo.ArticulosVO;

public interface ChatBotService {

	List<Long> getAllEmpresas();

	U01_FFResponseVO searchInInventario(String subcategoria, String categoria, String articulos, String colores,
			String marcas, String submarca, String tamanio);

	boolean addToCart(Long idarticulo, Long cantidad, Long idcarrito);

	ReceiptVO getCart(Long idcarrito);

}
