package com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates;

public class U01_FFButtonTemplateVO {
	private String type;
	private String text;
	private String postback;
	
	
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getPostback() {
		return postback;
	}
	public void setPostback(String postback) {
		this.postback = postback;
	}
	
	
}
