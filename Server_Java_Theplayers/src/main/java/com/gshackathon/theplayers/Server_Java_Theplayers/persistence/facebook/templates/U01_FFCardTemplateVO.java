package com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates;

import java.util.List;

public class U01_FFCardTemplateVO {
	private String title;
	private String imageUri;
	private String subtitle;
	private List<U01_FFButtonTemplateVO> buttons;
	
	
	
	public String getImageUri() {
		return imageUri;
	}
	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public List<U01_FFButtonTemplateVO> getButtons() {
		return buttons;
	}
	public void setButtons(List<U01_FFButtonTemplateVO> buttons) {
		this.buttons = buttons;
	}
	
	
	
}
