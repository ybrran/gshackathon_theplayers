package com.gshackathon.theplayers.Server_Java_Theplayers.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.vo.ArticulosVO;

@Mapper
public interface U01_CarritoMyBatisDAO {


	
	String ADDTOCART="INSERT INTO UNITIS01_24_CAR_PRODUC\n" + 
			"(carritocompras_id, articulos_id, st_activo, fh_creacion, fh_modificacion, usu_creo, usu_modifico, cantidad)\n" + 
			"VALUES(1, #{idarticulo}, 1, SYSDATE(), SYSDATE(), 1, 1, #{cantidad})";


	String GETCARRITOBYID = "SELECT \n" + 
			"	ART.id_articulo idarticulo, CART.cantidad, SUBCAT.nb_subcategoria subcategoria,\n" + 
			"	CAT.nb_categoria categoria, ART.nb_articulo articulos, COLOR.nb_color colores,\n" + 
			"	MARC.nb_marca marcas, SUBMARC.nb_submarca submarca, TAM.nb_tamanio tamanio,\n" + 
			"	ART.url_imagen, INV.precio\n" + 
			"FROM TPV019D_INVENTARIOS INV\n" + 
			"	JOIN TPV013C_ARTICULOS ART ON ART.id_articulo = INV.articulo_id\n" + 
			"	JOIN TPV009C_SUBCATEGORIAS SUBCAT ON SUBCAT.id_subcategoria = ART.subcategoria_id\n" + 
			"		JOIN TPV008C_CATEGORIAS CAT ON CAT.id_categoria = SUBCAT.categoria_id\n" + 
			"	JOIN TPV011C_SUBMARCAS SUBMARC ON SUBMARC.id_submarca = ART.submarca_id\n" + 
			"		JOIN TPV010C_MARCAS MARC ON MARC.id_marca = SUBMARC.marca_id\n" + 
			"	JOIN TPV012C_TAMANIOS TAM ON TAM.id_tamanio = ART.tamanio_id\n" + 
			"	JOIN TPV015C_COLORES COLOR ON COLOR.id_color = ART.color_id\n" + 
			"	JOIN UNITIS01_24_CAR_PRODUC CART ON CART.articulos_id = ART.id_articulo\n" + 
			"WHERE CART.carritocompras_id = #{idcarrito} ";
	
	String ARTICLEISONCART="SELECT articulos_id as idarticulo, cantidad "
			+ "FROM UNITIS01_24_CAR_PRODUC WHERE articulos_id = #{idarticulo} and carritocompras_id = #{idcarrito}";
	
	String UPDATECARRITO = "UPDATE UNITIS01_24_CAR_PRODUC\n" + 
			"SET fh_modificacion=SYSDATE(), cantidad=#{cantidad} \n" + 
			"WHERE carritocompras_id = 1 and articulos_id = #{idarticulo}";
	
	String SEARCHINCARRITO = "SELECT \n" + 
			"	ART.id_articulo AS idarticulo,  \n" + 
			"	ART.tipo AS tipo, COLOR.nombre AS color, SUBCAT.nombre AS subcategoria,\n" + 
			"	TALLA.nombre AS talla, MARCAS.nombre AS marca\n" + 
			"FROM UNITIS01_24_CAR_PRODUC CAR\n" + 
			"JOIN UNITIS01_09_ARTICULOS ART ON ART.id_articulo = CAR.articulos_id\n" + 
			"JOIN UNITIS01_08_SUBCATEGORIAS SUBCAT ON SUBCAT.id_subcategoria = ART.subcategorias_id\n" + 
			"JOIN UNITIS01_07_CATEGORIAS CAT ON CAT.id_categoria = SUBCAT.categorias_id\n" + 
			"JOIN UNITIS01_28_TALLAS_ART TALLART ON TALLART.articulos_id = ART.id_articulo\n" + 
			"JOIN UNITIS01_27_TALLAS TALLA ON TALLA.id_talla = TALLART.tallas_id\n" + 
			"JOIN UNITIS01_14_ARTIC_COLORES ARTCOLOR ON ARTCOLOR.articulos_id = ART.id_articulo\n" + 
			"JOIN UNITIS01_13_COLORES COLOR ON COLOR.id_color = ARTCOLOR.colores_id\n" + 
			"JOIN UNITIS01_15_MARC_SBM_ART MARCSUBART ON MARCSUBART.articulos_id = ART.id_articulo \n" + 
			"JOIN UNITIS01_11_MARCAS MARCAS ON MARCAS.id_marca = MARCSUBART.marcas_id";
	
	
	String DELETEFROMCARRITO = "DELETE FROM UNITIS01_24_CAR_PRODUC\n" + 
			"WHERE carritocompras_id = #{idcarrito} and articulos_id = #{idarticulo}";
	
	
	@Select(value = ADDTOCART)
	public void addToCart(@Param("idarticulo") Long idarticulo,@Param("cantidad") Long cantidad);
	
	@Select(value = SEARCHINCARRITO)
	public List<ArticulosVO> searchInCarrito();
	
	@Select(value = ARTICLEISONCART)
	public ArticulosVO articleIsOnCart(@Param("idarticulo") Long idarticulo,
			@Param("idcarrito") Long idcarrito);
	
	@Select(value = UPDATECARRITO)
	public ArticulosVO updateCarrito(@Param("idarticulo") Long idarticulo,@Param("cantidad") Long cantidad);
		
	@Select(value = DELETEFROMCARRITO)
	public void deleteArticleFromCarrito(@Param("idarticulo") Long idarticulo, @Param("idcarrito") Long idcarrito);
	
	@Select(value = GETCARRITOBYID)
	public List<ArticulosVO> getCart(@Param("idcarrito") Long idcarrito);

	
}
