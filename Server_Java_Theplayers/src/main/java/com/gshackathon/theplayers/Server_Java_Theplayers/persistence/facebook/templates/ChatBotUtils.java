package com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates;

import org.json.JSONException;
import org.json.JSONObject;

public class ChatBotUtils {

	
	private String iterateArray(String[] array, String jsonObject) throws JSONException {
		for(int i=0; i<array.length;i++) {
			JSONObject jsonObj = new JSONObject(jsonObject);
			jsonObject = jsonObj.optString(array[i]);
		}
		return jsonObject;
	}
	
	public String getIntentName_FromDlgFlwRequest_Util(String jsonObject) throws JSONException {
		String[] displayName = {"queryResult","intent","displayName"};	
		return iterateArray(displayName, jsonObject);
	}
	
	public String getTxtMessageFromDlgFlwRequest_Util(String jsonObject) throws JSONException {
		String[] displayName = {"queryResult","queryText"};	
		return iterateArray(displayName, jsonObject);
	}
	
	public String getParamFromDlgFlwRequest_Util(String jsonObject) throws JSONException {
		String[] displayName = {"queryResult","parameters"};	
		return iterateArray(displayName, jsonObject);
	}
	
	public String generateResponseTemplate_Util(String response) {
		return "{'fulfillmentText':"+response+"}";
	}
	
}
