package com.gshackathon.theplayers.Server_Java_Theplayers.rest.carrito;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates.ReceiptVO;
import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates.U01_FFResponseVO;
import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates.U03_ActionResponseVO;
import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.vo.ArticulosVO;
import com.gshackathon.theplayers.Server_Java_Theplayers.service.carrito.ChatBotService;

@RestController
public class ChatBotRestController {
	
	@Autowired
	private ChatBotService chatBotService;
	

	@RequestMapping(value = "/gs_getHola", method = RequestMethod.GET)
	@ResponseBody
	public String getHola() {
		System.out.println("hola GSHackathon home");
		return "hola GSHackathon home";
	}
	
	@RequestMapping(value = "/gs_empresas", method = RequestMethod.GET)
	@ResponseBody
	public List<Long> getEmpresas() {
		List<Long> empresas = chatBotService.getAllEmpresas();
		return empresas;
	}
	
	@RequestMapping(value = "/gs_searcharticulos", method = RequestMethod.POST)
	@ResponseBody
	public U01_FFResponseVO searcharticulos(@RequestBody ArticulosVO articulo) {

		U01_FFResponseVO articulos =chatBotService.searchInInventario(
				articulo.getSubcategoria(), articulo.getCategoria(),
				articulo.getArticulos(),articulo.getColores(),
				articulo.getMarcas(),articulo.getSubmarca(),
				articulo.getTamanio());
		return articulos;
	}
	
	@RequestMapping(value = "/gs_addToCart", method = RequestMethod.POST)
	@ResponseBody
	public U03_ActionResponseVO addToCart(
			@RequestBody ArticulosVO articulo) {
		U03_ActionResponseVO response = new U03_ActionResponseVO();
		Long idCarrito=1L;
		boolean result = chatBotService.addToCart(articulo.getIdarticulo(), articulo.getCantidad(),idCarrito);
		response.setEstatus(result);
		return response;
	}
	
	@RequestMapping(value = "/gs_getCart", method = RequestMethod.GET)
	@ResponseBody
	public ReceiptVO getCart(
			@RequestParam(value = "idcarrito") Long idcarrito
			) {
		
		return chatBotService.getCart(1L);
		//if(response.getFulfillmentMessages().size()<=0) {
		//	response.setFulfillmentMessages(null);
		//}
		//U03_ParamResponseVO res = new U03_ParamResponseVO();
		//res.setValparam(response);
		//return res;
	}
	
}
