package com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates;

public class AdjustmentsVO {
	private String name;
	private double amount;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
}
