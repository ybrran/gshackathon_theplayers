package com.gshackathon.theplayers.Server_Java_Theplayers.persistence.vo;

public class ArticulosVO {
	private Long idarticulo;
	private Long cantidad;
	private String subcategoria;
	private String categoria;
	private String articulos;
	private String colores;
	private String marcas;
	private String submarca;
	private String tamanio;
	private String url_imagen;
	private float precio;
	private int insearch =0;//Campo para filtrar si el elemento coincide con la busqueda
	public Long getIdarticulo() {
		return idarticulo;
	}
	public void setIdarticulo(Long idarticulo) {
		this.idarticulo = idarticulo;
	}
	public Long getCantidad() {
		return cantidad;
	}
	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}
	public String getSubcategoria() {
		return subcategoria;
	}
	public void setSubcategoria(String subcategoria) {
		this.subcategoria = subcategoria;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getArticulos() {
		return articulos;
	}
	public void setArticulos(String articulos) {
		this.articulos = articulos;
	}
	public String getColores() {
		return colores;
	}
	public void setColores(String colores) {
		this.colores = colores;
	}
	public String getMarcas() {
		return marcas;
	}
	public void setMarcas(String marcas) {
		this.marcas = marcas;
	}
	public String getSubmarca() {
		return submarca;
	}
	public void setSubmarca(String submarca) {
		this.submarca = submarca;
	}
	public String getTamanio() {
		return tamanio;
	}
	public void setTamanio(String tamanio) {
		this.tamanio = tamanio;
	}
	public String getUrl_imagen() {
		return url_imagen;
	}
	public void setUrl_imagen(String url_imagen) {
		this.url_imagen = url_imagen;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public int getInsearch() {
		return insearch;
	}
	public void setInsearch(int insearch) {
		this.insearch = insearch;
	}
	
	
	
	
	
	 
}
