package com.gshackathon.theplayers.Server_Java_Theplayers.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.gshackathon.theplayers.Server_Java_Theplayers.persistence.vo.ArticulosVO;

@Mapper
public interface ChatBotMyBatisDAO {

	String GETEMPRESAS = "SELECT * FROM TPV001D_EMPRESAS";
	
	String SEARCHINVENTORY = "SELECT \n" + 
			"	ART.id_articulo idarticulo, SUBCAT.nb_subcategoria subcategoria, \n" + 
			"	CAT.nb_categoria categoria, ART.nb_articulo articulos, COLOR.nb_color colores,\n" + 
			"	MARC.nb_marca marcas, SUBMARC.nb_submarca submarca, TAM.nb_tamanio tamanio,\n" + 
			"	ART.url_imagen, INV.precio\n" + 
			"FROM TPV019D_INVENTARIOS INV\n" + 
			"	JOIN TPV013C_ARTICULOS ART ON ART.id_articulo = INV.articulo_id\n" + 
			"	JOIN TPV009C_SUBCATEGORIAS SUBCAT ON SUBCAT.id_subcategoria = ART.subcategoria_id\n" + 
			"		JOIN TPV008C_CATEGORIAS CAT ON CAT.id_categoria = SUBCAT.categoria_id\n" + 
			"	JOIN TPV011C_SUBMARCAS SUBMARC ON SUBMARC.id_submarca = ART.submarca_id\n" + 
			"		JOIN TPV010C_MARCAS MARC ON MARC.id_marca = SUBMARC.marca_id\n" + 
			"	JOIN TPV012C_TAMANIOS TAM ON TAM.id_tamanio = ART.tamanio_id\n" + 
			"	JOIN TPV015C_COLORES COLOR ON COLOR.id_color = ART.color_id";
	
	String GETARTICLEBYID="SELECT \n" + 
			"	ART.id_articulo idart, SUBCAT.nb_subcategoria subcategoria, \n" + 
			"	CAT.nb_categoria categoria, ART.nb_articulo articulos, COLOR.nb_color colores,\n" + 
			"	MARC.nb_marca marcas, SUBMARC.nb_submarca submarca, TAM.nb_tamanio tamanio,\n" + 
			"	ART.url_imagen imagen, INV.precio\n" + 
			"FROM TPV019D_INVENTARIOS INV\n" + 
			"	JOIN TPV013C_ARTICULOS ART ON ART.id_articulo = INV.articulo_id\n" + 
			"	JOIN TPV009C_SUBCATEGORIAS SUBCAT ON SUBCAT.id_subcategoria = ART.subcategoria_id\n" + 
			"		JOIN TPV008C_CATEGORIAS CAT ON CAT.id_categoria = SUBCAT.categoria_id\n" + 
			"	JOIN TPV011C_SUBMARCAS SUBMARC ON SUBMARC.id_submarca = ART.submarca_id\n" + 
			"		JOIN TPV010C_MARCAS MARC ON MARC.id_marca = SUBMARC.marca_id\n" + 
			"	JOIN TPV012C_TAMANIOS TAM ON TAM.id_tamanio = ART.tamanio_id\n" + 
			"	JOIN TPV015C_COLORES COLOR ON COLOR.id_color = ART.color_id\n" + 
			"WHERE ART.id_articulo = #{idarticulo}";
	
	String ARTICLEISONCART="SELECT articulos_id as idarticulo, cantidad "
			+ "FROM UNITIS01_24_CAR_PRODUC WHERE articulos_id = #{idarticulo} and carritocompras_id = #{idcarrito}";
	
	@Select(value = GETEMPRESAS)
	public List<Long> getEmpresas();
	
	@Select(value = SEARCHINVENTORY)
	public List<ArticulosVO> searchInInventario();
	
	@Select(value = GETARTICLEBYID)
	public ArticulosVO getArtById(@Param("idarticulo") Long idarticulo);
	
	@Select(value = ARTICLEISONCART)
	public ArticulosVO articleIsOnCart(@Param("idarticulo") Long idarticulo,
			@Param("idcarrito") Long idcarrito);
	
}
