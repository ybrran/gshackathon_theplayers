package com.gshackathon.theplayers.Server_Java_Theplayers.persistence.facebook.templates;

import java.util.List;

public class ReceiptVO {
	
	private String template_type ="receipt";
	private String recipient_name;
	private String order_number;
	private String currency;
	private String payment_method;
	private String order_url;
	private String timestamp;
	private AddressVO address;
	private SummaryVO summary;
	private List<AdjustmentsVO> adjustments;
	private List<ElementsReceiptVO> elements;
	
	
	public String getTemplate_type() {
		return template_type;
	}
	public void setTemplate_type(String template_type) {
		this.template_type = template_type;
	}
	public String getRecipient_name() {
		return recipient_name;
	}
	public void setRecipient_name(String recipient_name) {
		this.recipient_name = recipient_name;
	}
	public String getOrder_number() {
		return order_number;
	}
	public void setOrder_number(String order_number) {
		this.order_number = order_number;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getPayment_method() {
		return payment_method;
	}
	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}
	public String getOrder_url() {
		return order_url;
	}
	public void setOrder_url(String order_url) {
		this.order_url = order_url;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public AddressVO getAddress() {
		return address;
	}
	public void setAddress(AddressVO address) {
		this.address = address;
	}
	public SummaryVO getSummary() {
		return summary;
	}
	public void setSummary(SummaryVO summary) {
		this.summary = summary;
	}
	public List<AdjustmentsVO> getAdjustments() {
		return adjustments;
	}
	public void setAdjustments(List<AdjustmentsVO> adjustments) {
		this.adjustments = adjustments;
	}
	public List<ElementsReceiptVO> getElements() {
		return elements;
	}
	public void setElements(List<ElementsReceiptVO> elements) {
		this.elements = elements;
	}
	
	

}
